from our_sum.my_sum.mypyfile import MyClass1
from tbears.libs.scoretest.score_test_case import ScoreTestCase


class TestMyClass1Again(ScoreTestCase):
    def setUp(self):
        super().setUp()

        self.myclass = MyClass1()

    def test_say_hello_again(self):
        result = self.myclass.sayHello()
        self.assertEqual(result, 'Hello')
        self.assertTrue(True)
        
