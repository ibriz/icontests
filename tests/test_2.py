import unittest

from our_sum.my_sum.mypyfile import MyClass1

class TestMyClass1(unittest.TestCase):

    myclass = MyClass1()

    def test_say_hello(self):
        """
        Test sayHello
        """
        result = self.myclass.sayHello()
        self.assertEqual(result, 'Hello')

if __name__ == '__main__':
    unittest.main()